import React from 'react';
import {shallow} from 'enzyme';
import App from './App';

const numTests = 1000;
const results = [];

afterAll(() => {
    return console.log(results);
});

describe('<App />', () => {

    describe('shallow in beforeEach', () => {
        const t0 = performance.now();
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<App/>);
        });
        for (let i = 0; i < numTests; i++) {
            it('renders without crashing', () => {
                expect(wrapper.exists()).toBe(true);
            });
        }
        const t1 = performance.now();
        results.push(`start shallow in beforeEach took ${t1 - t0}`);
    });

    describe('shallow in test', () => {
        const t0 = performance.now();
        let wrapper;
        for (let i = 0; i < numTests; i++) {
            it('renders without crashing', () => {
                wrapper = shallow(<App/>);
                expect(wrapper.exists()).toBe(true);
            });
        }
        const t1 = performance.now();
        results.push(`start shallow in test took ${t1 - t0}`);
    });

    describe('mock and shallow in beforeEach', () => {
        const t0 = performance.now();
        let wrapper;
        let m;
        beforeEach(() => {
            m = jest.fn();
            wrapper = shallow(<App/>);
        });
        for (let i = 0; i < numTests; i++) {
            it('renders without crashing', () => {
                expect(wrapper.exists()).toBe(true);
            });
        }
        const t1 = performance.now();
        results.push(`start shallow with mock in beforeEach took ${t1 - t0}`);
    });

    describe('mockClear in beforeEach; shallow in test', () => {
        const t0 = performance.now();
        let wrapper;
        const m = jest.fn();

        beforeEach(() => {
            m.mockClear();
        });

        for (let i = 0; i < numTests; i++) {
            it('renders without crashing', () => {
                wrapper = shallow(<App/>);
                expect(wrapper.exists()).toBe(true);
            });
        }
        const t1 = performance.now();
        results.push(`start shallow with mock in test took ${t1 - t0}`);
    });

    describe('shallow and mockClear() in beforeEach', () => {
        const t0 = performance.now();
        let wrapper;
        const m = jest.fn();

        beforeEach(() => {
            m.mockClear();
            wrapper = shallow(<App/>);
        });

        for (let i = 0; i < numTests; i++) {
            it('renders without crashing', () => {
                expect(wrapper.exists()).toBe(true);
            });
        }
        const t1 = performance.now();
        results.push(`shallow and mockClear() in beforeEach took ${t1 - t0}`);
    });
});



